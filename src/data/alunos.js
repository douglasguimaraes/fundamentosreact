const alunos = [
    {id: 1, nome: 'Ana', nota: 9.9},
    {id: 2, nome: 'Bia', nota: 7.0},
    {id: 3, nome: 'Daniel', nota: 5.4},
    {id: 4, nome: 'José', nota: 6.8},
    {id: 5, nome: 'Flávio', nota: 7.9},
    {id: 6, nome: 'Arthur', nota: 9.2},
    {id: 7, nome: 'Rebeca', nota: 7.4},
    {id: 8, nome: 'Guilherme', nota: 5.1},
    {id: 9, nome: 'Gustavo', nota: 7.4},
];

export default alunos;