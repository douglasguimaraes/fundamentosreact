const produtos = [
    { id: 1, nome: 'Produto 1', preco: 9.90 },
    { id: 2, nome: 'Produto 2', preco: 15.00 },
    { id: 3, nome: 'Produto 3', preco: 20.00 },
]

export default produtos;