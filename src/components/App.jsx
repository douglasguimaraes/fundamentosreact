import './App.css'
import React from 'react'

import UsuarioInfo from './condicional/UsuarioInfo'
import ParOuImpar from './condicional/ParOuImpar'
import ListaProdutos from './repeticao/ListaProdutos'
import ListaAlunos from './repeticao/ListaAlunos'
import Familia from './basicos/Familia'
import FamiliaMembro from './basicos/FamiliaMembro'
import Card from './layout/Card'
import NumeroAleatorio from './basicos/NumeroAleatorio'
import Fragmento from './basicos/Fragmento'
import ComParametro from './basicos/ComParametro'
import Primeiro from './basicos/Primeiro'

const app = () => {
    
    return (
        <div className="App">
            <h1>Fundamentos React</h1>

            <div className="Cards">

                <Card titulo="#08 - Condicional" color="#HH4C65">
                    <ParOuImpar numero={21} />
                    <UsuarioInfo usuario={{ nome: 'Douglas'}} />
                    <UsuarioInfo usuario={{ email: 'Douglas'}} />
                </Card>

                <Card titulo="#07 - Repetição #2" color="#000">
                    <ListaProdutos />
                </Card>

                <Card titulo="#06 - Repetição #1" color="#FF4C65">
                    <ListaAlunos />
                </Card>

                <Card titulo="#05 - Componente com Filhos" color="#00C8F8">
                    <Familia sobrenome="Guimarães">
                        <FamiliaMembro nome="Douglas" />
                        <FamiliaMembro nome="Ana" />
                        <FamiliaMembro nome="Ariane" />
                    </Familia>
                </Card>

                <Card titulo="#04 - Desafio Aleatório" color="#FA6900">
                    <NumeroAleatorio min={1} max={256}></NumeroAleatorio>
                </Card>

                <Card titulo="#03 - Fragmento" color="#E94C6F">
                    <Fragmento></Fragmento>
                </Card>

                <Card titulo="#02 - Com Parâmetro" color="#E8B71A">
                    <ComParametro titulo="Segundo Componente"
                        aluno="Pedro"
                        nota={9.3}
                    />
                </Card>

                <Card titulo="#01 - Primeiro Componente" color="#588C73">
                    <Primeiro></Primeiro>
                </Card>

            </div>
        </div>
    );
}

export default app;
    