import React from 'react'

const parOuImpar = (props) => {

    const isPart = props.numero % 2 === 0;
    return (
        <>
            {isPart ? <span>Par</span> : <span>Ímpar</span>}
        </>
    )
};

export default parOuImpar;