import React from 'react'

const familiaMembro = (props) => {
    return (
        <div>
            <span>
                {props.nome} <strong>{props.sobrenome}</strong>
            </span>
        </div>
    );
}

export default familiaMembro;