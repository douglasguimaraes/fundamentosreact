import React from 'react'

const numeroAleatorio = (props) =>{
    const { min, max } = props;
    const numero = RandomNumber(min, max);
    return (
        <div>
            <h2>Número Aleatório</h2>
            Número aleatório entre {min} e {max}: <strong>{numero}</strong>
        </div>
    );
}

export default numeroAleatorio;

function RandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}